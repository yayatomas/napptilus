import {useEffect, useState} from 'react';
import moment from 'moment';

export const useStorage = (
  key,
  days,
  callback,
  arg,
) => {
  const [update, setUpdate] = useState(false)
  useEffect(() => {
    (async () => {
      let data = await JSON.parse(window.localStorage.getItem(key));
      if(data){
        const endDate = moment();
        const diff = moment(endDate).diff(moment(data.time), 'days');
        diff > days && setUpdate(true)
      }
      if(!data || update) {
       const response = await callback(arg)
        const storage = JSON.stringify({
          time: moment(),
          data: response
        })
       data = await window.localStorage.setItem(key, storage)
      }
    })()
  },[key, days, callback, update, arg])
}