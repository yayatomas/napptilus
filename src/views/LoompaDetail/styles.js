import styled from 'styled-components';

export const DetailWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 200px 100px;
  margin: 0 auto;
`