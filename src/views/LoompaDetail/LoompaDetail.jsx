import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {object, func} from 'prop-types';
import {getLoompaById} from '../../redux/actions/actions';
import DetailCard from '../../components/DetailCard/DetailCard';
import {DetailWrapper} from './styles';
import Loader from '../../components/Loader/Loader';

const LoompaDetail = ({
  match,
  history,
  getLoompaById,
  loompa,
  isLoading,
}) => {
  const {id} = match.params;

  useEffect(() => { 
    id && getLoompaById(id)
  }, [getLoompaById, id]);
  
  useEffect(() => {
    !Number(id) && history.push('/')
  },[history, id]);
  
  const content = isLoading ? <Loader /> : <DetailCard {...loompa} />;
  return(
    <DetailWrapper>
      {content}
    </DetailWrapper>
  )
}

const mapStateToProps = ({
  loompas: {
    loompa,
    isLoading
  },
}) => ({loompa, isLoading});

const mapDispatchToProps = {
  getLoompaById,
}

export default connect(mapStateToProps, mapDispatchToProps)(LoompaDetail);

LoompaDetail.propTypes = {
  match: object.isRequired,
  history: object.isRequired,
  getLoompaById: func.isRequired,
  loompa: object.isRequired,
}