import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 1000px;
  flex-wrap: wrap;
  margin: 10px auto;
  overflow-x: none;
`

export const Header = styled.div`
  height: 200px;
  width: 100%;
  text-align: center;
  margin: 0;
`

export const SearchContainer = styled.div`
  height: 150px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding-right: 300px;
  padding-top: 50px;
`

export const Title = styled.h2`
  font-size: 2em;
`
export const Subtitle = styled.p`
  font-size: 1.5em;
  color: lightgray;
`