import React from 'react';
import {connect} from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { getAllLoompas, searchLoompa } from '../../redux/actions/actions';
import LoompaCard from '../../components/LoompaCard/LoompaCard';
import SearchBar from '../../components/SearchBar/SearchBar';
import {
  Container,
  Header,
  SearchContainer,
  Title,
  Subtitle,
} from './styles';

const Home = ({
  currentLoompas,
  getAllLoompas,
  searchLoompa,
}) => {
  const displayLoompas = currentLoompas.map(({id, ...loompa}) => <LoompaCard key={id} id={id} {...loompa} />)
  return(
    <InfiniteScroll
      loadMore={e => getAllLoompas(e)}
      hasMore={true}
      loader={<div></div>}
    >
      <SearchContainer>
        <SearchBar
          placeholder='Search'
          onChange={e => searchLoompa(e.target.value)}
        />
      </SearchContainer>
      <Header>
        <Title>Find your Oompa Loompa</Title>
        <Subtitle>There are more than a 100k</Subtitle>
      </Header>
      <Container>
        {displayLoompas}
      </Container>
    </InfiniteScroll>
  )
};

const mapStateToProps = ({loompas: {currentLoompas}}) => ({currentLoompas})
const mapDispatchToProps = {
  getAllLoompas,
  searchLoompa,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);