import {
  compose, 
  combineReducers, 
  createStore, 
  applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';
import {loompasReducer} from '../redux/reducer/loompasReducer';

const rootReducer = combineReducers({
  loompas: loompasReducer,
});

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : null;

export const store = createStore(
    rootReducer, 
    compose(
    applyMiddleware(thunk),
    devTools
  )
);
