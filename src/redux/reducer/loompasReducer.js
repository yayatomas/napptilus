import {
  GET_LOOMPA, 
  GET_ALL_LOOMPAS, 
  SEARCH_LOOMPA, 
  LOADING,
} from '../actions/actionTypes';

const inisitalState = {
  currentLoompas: [],
  stashLoompas: [],
  loompa: {},
  isLoading: true,
}

export const loompasReducer = (state = inisitalState, action) => {
  const {type, payload} = action;
  switch(type) {
    case GET_ALL_LOOMPAS: 
      return {
        ...state,
        currentLoompas: [...state.currentLoompas, ...payload],
        stashLoompas: [...state.stashLoompas ,...payload],
      }
    case GET_LOOMPA: 
      return {
        ...state,
        loompa: payload
      }
    case SEARCH_LOOMPA:
      return {
        ...state,
        currentLoompas: [...state.stashLoompas].filter(loompa => loompa.first_name.includes(payload) || loompa.last_name.includes(payload))
      }
    case LOADING:
      return {
        ...state,
        isLoading: false
      }
    default:
      return {
        ...state
      }
  }
};

