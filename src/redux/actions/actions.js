import loompaService from '../../service/LoompaService';
import {
  GET_ALL_LOOMPAS,
  GET_LOOMPA,
  SEARCH_LOOMPA,
  LOADING,
} from './actionTypes';

export const getAllLoompas =  page => async dispatch => {
  try {
    const {results} = await loompaService.getAllLoompas(page);
    dispatch({
      type: GET_ALL_LOOMPAS,
      payload: results
    });
  }
  catch(e){
    console.log(e)
  }
};

export const getLoompaById = id => async dispatch => {
  try {
    const loompa = await loompaService.getLoompaById(id);
    dispatch({
      type: GET_LOOMPA,
      payload: loompa
    });
    dispatch({
      type: LOADING,
      payload: false,
    })
    return loompa
  }
  catch(e){
    console.log(e)
  }
};

export const searchLoompa = str => dispatch => {
  dispatch({
    type: SEARCH_LOOMPA,
    payload: str,
  })
}