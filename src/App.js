import React from 'react';
import {store} from './redux/store';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from './views/Home/Home';
import LoompaDetail from './views/LoompaDetail/LoompaDetail';
import Header from './components/Header/Header';
import './App.css'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Switch>
          <Route path='/loompa/:id' component={LoompaDetail} />
          <Route exact path='/' component={Home} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
