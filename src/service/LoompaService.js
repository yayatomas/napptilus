import axios from 'axios';

class LoompaService {
  constructor(){
    this.api = axios.create({
      baseURL: 'https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas',
    });
  }

  getAllLoompas = async page => {
    try {
      const {data} = await this.api.get(`?page=${page}`);
      return data
    }
    catch(e) {
      console.log(e)
    }
  };

  getLoompaById = async id => {
    try {
      const {data} = await this.api.get(`/${id}`)
      return data
    }
    catch(e) {
      console.log(e)
    }
  }
}

const loompaService = new LoompaService();
export default loompaService;