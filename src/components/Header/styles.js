import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  padding: 10px;
  margin: -10px 0 0 0;
  z-index: 10;
  position: fixed;
  background: lightgray;
  height: 50px;
  box-shadow: 2px 6px 18px 0px rgba(0,0,0,0.48);
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 100px;
`

export const Image = styled.img`
  width: 25px;
  height: 20px;
  opacity: 0.6;
  padding-top: 10px;
`