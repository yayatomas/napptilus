import React from 'react';
import {Link} from 'react-router-dom';
import {Container} from './styles';
import {Image} from './styles';

const Header = () => {
  return(
    <Container>
      <Link to='/'>
        <Image
          src='https://s3.eu-central-1.amazonaws.com/napptilus/level-test/imgs/logo-umpa-loompa.png' 
          alt='logo' 
        />
      </Link>
    </Container>
  )
};

export default Header;