import React from 'react';
import {Link} from 'react-router-dom';
import {string, number} from 'prop-types';
import {useLazyLoading} from '../../hooks/useLazyLoading';
import {
  Container, 
  Wrapper,
  Image,
  LinkText,
  Text,
} from './styles';

const LoompaCard = ({
  profession,
  first_name,
  last_name,
  gender,
  image,
  id,
}) => {
  const [show, element] = useLazyLoading()
  const genderName = gender === 'F' ? 'Woman' : 'Man';
  return(
    <Wrapper ref={element}>
      { 
        show && 
          <Container>
            <Image src={image} alt={`${first_name} ${last_name}`}/>
            <Link to={`/loompa/${id}`} style={{ textDecoration: 'none' }}>
              <LinkText>
                {`${first_name} ${last_name}`}
              </LinkText>
            </Link>
            <Text>
              {genderName}
            </Text>
            <Text>
              {profession}
            </Text>
          </Container>
      }
    </Wrapper>
  )
};

LoompaCard.propTypes = {
  profession: string.isRequired,
  first_name: string.isRequired,
  last_name: string.isRequired,
  gender: string.isRequired,
  image: string.isRequired,
  id: number.isRequired,
}

export default LoompaCard;