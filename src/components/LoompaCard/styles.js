import styled, {keyframes} from 'styled-components';

const showAnimtion = keyframes`
  0% {
    opacity: 0 
  }
  50% {
    opacity: 0.6
  }
  100% {
    opacity: 1
  }
`
export const Container = styled.div`
  display: 'flex';
  flex-direction: 'column';
  justify-content: 'flex-start';
  background: 'none';
  width: 300px;
  padding: 0;
  margin: '10px';
  animation: ${showAnimtion} 2s ease-in;
`

export const Image = styled.img`
  width: 300px;
  height: 200px;
`

export const Wrapper = styled.div`
  width: 300px;
  height: 400px;
  margin: 10px;
  padding: 0;
`

export const LinkText = styled.h2`
  color: black;
  :hover {
    color: lightblue;
  }
`

export const Text = styled.p`
  color: lightgray
`



