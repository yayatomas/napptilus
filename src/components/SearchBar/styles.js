import styled from 'styled-components';

export const Input = styled.input`
  padding: 10px;
  border: none;
  border-radius: 5px;
  width: 100px;
  opacity: 0.7;
  background: #F8F8F8;
`