import React from 'react';
import {Input} from './styles';
import {func} from 'prop-types'

const SearchBar = ({...props}) => <Input {...props} />

export default SearchBar;

SearchBar.propTypes = {
  onChange: func.isRequired,
}