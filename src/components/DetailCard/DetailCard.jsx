import React from 'react';
import {string} from 'prop-types';
import {
  Container, 
  Content, 
  Title, 
  Description, 
  Image
} from './styles';

const DetailCard = ({
  first_name,
  last_name,
  gender,
  image,
  profession,
  description,
}) => {
  const genderName = gender === 'M' ? 'Male' : 'Female';
  return(
    <Container>
      <div>
        <Image src={ image } alt={`${first_name} ${last_name}`} />
      </div>
      <Content>
        <Title>{`${first_name} ${last_name}`}</Title>
        <p>{genderName}</p>
        <p>{profession}</p>
        <Description>{description}</Description>
      </Content>
    </Container>
  )
}

export default DetailCard;

DetailCard.propTypes = {
  first_name: string.isRequired,
  last_name: string.isRequired,
  gender: string.isRequired,
  image: string.isRequired,
  profession: string.isRequired,
  description: string.isRequired,
}