import styled, {keyframes} from 'styled-components';

const showAnimtion = keyframes`
  0% {
    opacity: 0 
  }
  50% {
    opacity: 0.6
  }
  100% {
    opacity: 1
  }
`

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 300px;
  width: 800px;
  animation: ${showAnimtion} 2s ease-in;
`

export const Image = styled.img`
  height: 310px;
  width: 300px;
`

export const Content = styled.div`
  font-size: 0.8em;
  color: lightgray;
  padding: 20px;
`

export const Title = styled.p`
  color: black;
  font-size: 1.5em;
`

export const Description = styled.p`
  color: black;
`
