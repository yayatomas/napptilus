
Oompa Loompas SPA.

Header: In both views fixed header is present with icon on the left, user can click the icon to be redirected to Home page.
Views:
 Home:
  Components:
    Search Bar: User can use the search bar to look for your favorite Oompa Loompa. To can look by first or last name.
    Infinite-Scroll: User can scroll down, component will keep rendering more Oompa Loompas till the end of the 100k.
      Components: 
        LoompaCard: Loompa card displays user the image, first name, last name, profession and gender of the Loompa. User can click on the Loompa's name to be re directed to the details view of that particular loompa.
  
LoompaDetail: 
  In LoompaDetail's view, if user access the url without Loompa's id it would be redirected to Home page.
  Components:
    Loader: user will see purple spinner while the page is loading its content. 
    DetailCard: DetailCard displays selected Loomp's image, first name, last name, profession, gender and descripition.

Instruction:
  For running the spa please clone the repo, do npm install and run in root directory npm start. Spa will run by default in localhost:3000.

